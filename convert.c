#include <stdio.h>
#include <stdint.h>
#include <stdlib.h> //use malloc for array assignment?
#include <math.h>

#include </home/pi/Desktop/C/Neu/bmp.h>     //bmpheader struct for later use

/**************unnecessary
#include </usr/include/IL/il.h>
#include </usr/include/IL/ilu.h>
#include </usr/include/IL/ilu_region.h>
*/


unsigned char* readBMP(char* filename, int toffset)
{
    
    int i, i2;
    int jump;

    FILE* f = fopen(filename, "r+b"); //open in binary mode for reading at 0
    if (f == NULL)  //catch 
    {
        printf("Zugriffsfehler. Datei nicht gefunden?\n");
        return;
    }

    unsigned char info[54];
    fread(info, sizeof(unsigned char), 54, f); // read 54-byte header


    // width:18 | height:22
    int width = *(int*)&info[18];
    int height = *(int*)&info[22];

    int size = 3 * width * height; //pixel grid multiplied by 3 for 24bit per pixel
    unsigned char data[size]; 
    
    jump=54;        // jump after the bmpheader(54)
    fseek(f, jump, SEEK_SET);   
    fread(data, sizeof(unsigned char), height*width*3, f);  
    fclose(f);

/* fseek()
Constant    Reference position
SEEK_SET    Beginning of file
SEEK_CUR    Current position of the file pointer
SEEK_END    End of file *
*/


    printf("%s%d\n","width:", width);
    printf("%s%d\n", "height:", height);
    printf("%s%d\n", "size:", size,"\n");

    int16_t x_[3*width*height];
    int16_t y_[3*width*height];
    int pass = 0;
/******************* comparison *************************/


/*  int i;
    
    // -- FILE HEADER -- //

    // bitmap signature
    bitmap[0] = 'B';
    bitmap[1] = 'M';

    // file size
    bitmap[2] = 66; // 40 + 14 + 12
    bitmap[3] = 0;
    bitmap[4] = 0;
    bitmap[5] = 0;

    // reserved field (in hex. 00 00 00 00)
    bitmap[6] = 0;
    bitmap[7] = 0;
    bitmap[8] = 0;
    bitmap[9] = 0;
    //for(i = 6; i < 10; i++) bitmap[i] = 0;

    // offset of pixel data inside the image
    bitmap[10] = 54;
    bitmap[11] = 0;
    bitmap[12] = 0;
    bitmap[13] = 0;
    //for(i = 10; i < 14; i++) bitmap[i] = 0; 

    // -- BITMAP HEADER -- //

    // header size
    bitmap[14] = 40;
    for(i = 15; i < 18; i++) bitmap[i] = 0;

    // width of the image
    bitmap[18] = 4;
    for(i = 19; i < 22; i++) bitmap[i] = 0;

    // height of the image
    bitmap[22] = 1;
    for(i = 23; i < 26; i++) bitmap[i] = 0;

    // reserved field
    bitmap[26] = 1;
    bitmap[27] = 0;

    // number of bits per pixel
    bitmap[28] = 24; // 3 byte
    bitmap[29] = 0;

    // compression method (no compression here)
    for(i = 30; i < 34; i++) bitmap[i] = 0;

    // size of pixel data
    bitmap[34] = 12; // > 4 pixels
    bitmap[35] = 0;
    bitmap[36] = 0;
    bitmap[37] = 0;

    // horizontal resolution of the image - pixels per meter (2835)
    bitmap[38] = 0;
    bitmap[39] = 0;
    bitmap[40] = 0b00110000;
    bitmap[41] = 0b10110001;

    // vertical resolution of the image - pixels per meter (2835)
    bitmap[42] = 0;
    bitmap[43] = 0;
    bitmap[44] = 0b00110000;
    bitmap[45] = 0b10110001;

    // color pallette information
    for(i = 46; i < 50; i++) bitmap[i] = 0;

    // number of important colors
    for(i = 50; i < 54; i++) bitmap[i] = 0;

    // -- PIXEL DATA -- //
    for(i = 54; i < 66; i++) bitmap[i] = 255;
*/
    // header
    //fseek (f, 0, SEEK_SET);
//  fwrite(info, sizeof(unsigned char), 54, f);

/*

///
    f = fopen(filename, "r+b"); //open in binary mode for reading at 0



    
    for(i = 0; i < (height*width*3); i+=3) 
    {   //jump to every pixel's B-byte
        char blue_ = data[i];
        char green_ = data[i+1];
        char red_ = data[i+2];

        if (blue_ > toffset && green_ > toffset && red_ > toffset)
        {
            data[i] = 255;
            data[i+1] = 255;
            data[i+2] = 255;
        }
        else
        {
            data[i] = 0;
            data[i+1] = 0;
            data[i+2] = 0;
        }
    }


    // skip header 
    fseek (f, 54, SEEK_SET);
    fwrite(data, sizeof(unsigned char), height*width*3, f);
    fclose(f);
*/


    f = fopen(filename, "r+b");

    // skip header
    fseek(f, 54, SEEK_SET);  

    int x, y;
    unsigned char row[width*3]; 
    // read line for line
    for(y = 0; y <= height; y++)
    {
        //printf("row y = %i\n", y);
        fread(row, sizeof(unsigned char), width*3, f); 
    
        for(i = 0; i <= sizeof(row); i+=3) 
        {
            //jump to every pixel's B-byte
            char blue_ = row[i];
            char green_ = row[i+1];
            char red_ = row[i+2];

            if (blue_ > toffset && green_ > toffset && red_ > toffset)
            {
                // pixel found
                pass++;

               // printf("Pass = %i\n", pass);

                row[i] = 255;
                row[i+1] = 255;
                row[i+2] = 255;

                x_[pass] = i; 
                y_[pass] = y;              
            }
            else
            {
                row[i] = 0;
                row[i+1] = 0;
                row[i+2] = 0;
            }
            //printf("blue = %i, green = %i, red = %i\n", row[i], row[i+1], row[i+2]);
        }
        fseek (f, -(width*3), SEEK_CUR);
        fwrite(row, sizeof(unsigned char), width*3, f);
    }



    printf("Gefundene Pixel: %i\n", pass);

    for(i = 1; i <= pass; i++)
    {
        printf("Table: %i|%i\n", x_[i], y_[i]);
    }

    x = 0;
    y = 0;

    for(i = 1; i <= pass; i++)
    {
        x += x_[i];
        y += y_[i];
    }

   // printf("sum x = %i, sum y = %i \n", x, y);



    


    x = x / 3;
    x = x / pass;
    y = y / pass;
    y = height - y;


    printf("Gefundene Pixel: %i\n", pass);
    printf("width: %i, height: %i\n", width, height);
    printf("zentraler Punkt: %i|%i\n", x, y);

    


    fclose(f);


    return 0;

}


unsigned char main (int argc, char *argv[])
{
    if(argv[1]==NULL || argv[2] == NULL) {
        printf("%s", "Usage: convert filename schwellwert\n");
        return(1);
    }
    readBMP(argv[1], atoi(argv[2]));

//  system("/home/pi/Desktop/C/Neu/raspifastcmd/do_caputure.sh");
    


    
    return(0);
}


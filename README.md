# README #

I think the default UART clock is 3MHz. The maximum baud rate is clock/16.

Try adding the following line to /boot/config.txt and rebooting.

init_uart_clock=4000000
    (250k baud x16)

--------------------------------------------------
Entfernungskonstansten:
A= 4500
B= -18

Entfernung = A / (messwert - B)